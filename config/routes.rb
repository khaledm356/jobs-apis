Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  # devise_for :admin_users, ActiveAdmin::Devise.config
  # scope :api, defaults: { format: :json } do
  # devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  scope :api, defaults: { format: :json } do
    devise_for :seekers, controllers: { sessions: :sessions },
                       path_names: { sign_in: :login }
  end
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :job_posts, only: %i[index show create] do 
        resource :jop_applications, only: %i[create]
        get '/jop_applications/:id' => 'jop_applications#show'
      end
    end
  end
  devise_scope :seeker do
    resources :registrations, only: %i[create]
  end
  # devise_for :seeker
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
