class CreateJobPosts < ActiveRecord::Migration[6.1]
  def change
    create_table :job_posts do |t|
      t.references :admin_user, null: false, foreign_key: true
      t.string :title
      t.text :description
      t.datetime :expiry_date
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
