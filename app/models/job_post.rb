class JobPost < ApplicationRecord
  belongs_to :admin_user
  has_many :jop_applications

  enum status: { published: 0, closed: 1 }

  validates :admin_user, presence: true
  validates :title, presence: true
  validates :description, presence: true
  validates :description, presence: true, length: { maximum: 2200 }

  attribute :expiration_date, :datetime, default: -> { Time.zone.now + 30.days }
end
