class AdminUser < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
       devise :database_authenticatable, 
              :recoverable, :rememberable, :validatable
       # validate :only_model

       # def only_model
       #        self.errors.add(:base, "One Admin account only can be added") if self.new_record? && AdminUser.count > 1
       # end
end
