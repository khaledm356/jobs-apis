class JopApplication < ApplicationRecord
  belongs_to :job_post
  belongs_to :seeker

  validates :seeker, presence: true
  validates :job_post, presence: true
  validates :title, presence: true
end
