ActiveAdmin.register JopApplication do
  
    index do
      selectable_column
      id_column
      column :title
      column :job_post
      column :seeker
      column :seen
      column :created_at
      column :updated_at
      actions
    end

    show do |article|
        attributes_table do
          row :id
          row :title
          row :job_post
          row :seeker
          row :seen
          row :created_at
          row :updated_at
        end
        active_admin_comments
    end

    def show
      super
      @jop_application.seen = true if @jop_application.false?
    end
end
  