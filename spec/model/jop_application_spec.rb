require 'rails_helper'

RSpec.describe JopApplication, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:seeker) }
    it { is_expected.to belong_to(:job_post)}
  end
  describe 'validations' do
    it { is_expected.to validate_presence_of(:seeker) }
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:job_post) }
  end
end