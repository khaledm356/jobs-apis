require 'rails_helper'

RSpec.describe JobPost, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:admin_user) }
    it { is_expected.to have_many(:jop_applications)}
  end
  describe 'validations' do
    it { is_expected.to validate_presence_of(:admin_user) }
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:description) }
    it { is_expected.to validate_length_of(:description).is_at_most(2200) }
  end
end