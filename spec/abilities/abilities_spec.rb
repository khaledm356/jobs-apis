require "cancan/matchers"
# ...
describe 'abilities' do
  let(:seeker) { FactoryBot.create(:seeker) }
  let(:admin_user) { FactoryBot.create(:admin_user) }
  let(:jop_application) { FactoryBot.create(:jop_application, seeker: seeker) }
  let(:jop_application1) { FactoryBot.create(:jop_application) }
  let(:ability) { FactoryBot.create(:user) }

  subject(:ability) { Ability.new(seeker) }
  subject(:admin_ability) { Ability.new(admin_user) }

  it 'should expect seekers abilities' do
    expect(ability).not_to be_able_to(:manage, JobPost)
    expect(ability).to be_able_to(:show, jop_application)
    expect(ability).not_to  be_able_to(:manage, JobPost)
    expect(ability).to_not be_able_to(:show, jop_application1)
  end

  it 'should expect adminusers abilities' do
    expect(admin_ability).to  be_able_to(:manage, JobPost)
    expect(admin_ability).to  be_able_to(:show, JopApplication)
    expect(admin_ability).to_not be_able_to(:create, JopApplication)
  end
end