require 'swagger_helper'

RSpec.describe 'api/v1/job_posts', type: :request do
  before do
    @seeker = FactoryBot.create(:seeker)
    @job_posts = FactoryBot.create_list(:job_post, 4)
    @auth_headers = @seeker.generate_jwt
    @headers = {
      'CONTENT_TYPE' => 'application/json',
      'ACCEPT' => 'application/json',
      'Authorization' => "Bearer #{@auth_headers}",
    }
  end

  path "/api/v1/job_posts/" do
    get 'listing job post' do
      tags 'Jop posts index'
      consumes 'application/json'
      produces 'application/json'
      response '200', 'job posts listed succsessfully' do
        it "should list job posts" do
          get api_v1_job_posts_path, headers: @headers
          expect(response).to have_http_status(:success)
          body = JSON.parse(response.body)
          expect(body.size).to eq(JobPost.where(status: 0).size)
        end
      end

      response '401', "unauthorized see job posts list" do
        it "Should not be able to list job posts" do
          get api_v1_job_posts_path
          expect(response).to have_http_status(401)
        end
      end
    end
  end
end
