# frozen_string_literal: true

FactoryBot.define do
  factory :seeker do
    email           { FFaker::Internet.email }
    password        { FFaker::Lorem.characters(6) }
  end
end
